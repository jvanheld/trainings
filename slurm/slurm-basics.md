template: title

# The SLURM Batch system

---

template: content

# What is a Batch system

The IFB cluster uses a Batch system software, a scheduler, for managing and allocating resources.

--

There are different batch systems, e.g.:

* Sun Grid Engine (SGE)
* Load Sharing Facility (LSF)
* TORQue
* SLURM

--

.center.callout.callout-success[**The IFB cluster is based on SLURM.**]

---

template: content

# SLURM components

A cluster based on SLURM is composed of several type of nodes:

--

* **Submission node**: this is the node where user connect to use the cluster (core.cluster.france-bioinformatique)

.center.callout.callout-warning[You should NEVER run programs on the submission node]

--

* **Compute nodes**: these are the nodes where you jobs get executed (lots of CPU and RAM)

--

* **Administrative node**: this is the node dedicated to the cluster management (you should not care about them)

--

* **Storage nodes**: these nodes are used to made your data available all over the cluster

---

# SLURM components

.center[![SLURM components](images/slurm_components.png)]

---

# Basic SLURM concepts

**Account**: A logical group of users

Resource consumption is associated with an account.<br/>
On the IFB cluster an account is created for each project.<br/>
You may have access to multiple account (if you have several projects).<br/>
Your first project will be your default account.

--

**Ressources**: nodes, CPUs, memory

--

**Partition**: logical group of nodes

They are two partitions on the IFB cluster:
* `fast`: limited to 1 day - 71 nodes available
* `long`: limited to 30 days - 21 nodes available
* `bigmem`: limited to 60 days - 1 node available - On demand
* `training`: limited to 1 day - 5 nodes available - On demand

The default partition is `fast`.

---

# Basic SLURM concepts

## Job

**User's point of view**: a calculation or data analysis

It could be a single program or a full pipeline (a succession of programs).

**Slurm's point of view**: an allocation of resources

--

## Job steps

The processes that actually do the real work

--

## Process/Task

An instance of a computer program executed by one or many threads

--

## Thread

A thread of execution is the smallest sequence of programmed instructions that can be executed

---

# Basic SLURM concepts

.center[![A job](images/a_job.drawio.png)]

---

# Basic SLURM concepts

.center[![A job](images/processes.drawio.png)]

---

# Basic SLURM concepts

.center[![A job](images/threads.drawio.png)]

---

# Basic SLURM concepts

.center[![A job](images/multijobs.drawio.png)]

---

# Submitting a job

They are two commands to let you submit a job to the cluster:

.left-column[
## `srun`
**Run job interactively**

* Outputs are returned to the terminal
* You have to wait until the job has terminated before starting a new job
* Works with **ANY command**

```
$ srun fastqc SRR1205973_extract_R1.fq.gz
```
]

--

.right-column[
## `sbatch`
**Batch job submission**

* Only returns the job id
* Runs in "background" on the cluster
* Outputs are sent to file(s)
* Works **ONLY with shell scripts**

```
$ sbatch myscript.sh
```
]

---

layout: true
name: submit-options

# Submitting a job

## Common options

---

template: submit-options

### CPUs  *(1 CPU = 1 hyperthreaded core)*

`--nodes/-N`: number of nodes (default is 1)

`--ntasks/-n`: number of tasks (default is 1)

`--cpus-per-task/-c`: number of CPU per task (default is 1)

--

**Examples**<br/>
```
$ srun hostname
cpu-node-1
$ srun --ntasks 2 hostname
cpu-node-1
cpu-node-1
$ srun --nodes 2 --ntasks 2 hostname
cpu-node-1
cpu-node-2
```

---

### Memory

`--mem`: memory for the whole job

`--mem-per-cpu`: memory per CPU

---

template: content

# Batch job

## What is a batch job ?

* An asynchronous action
* A job based on a script

--

.left-column[
## Use the `sbatch` command

`sbatch myscript.sh`

The script contains srun commands<br/>
Each `srun` is a job step
]

--

.right-column[
## Some `sbatch` options

`-o -e`: redirect job output/error in different file
]
---
template: content

# Batch script rules

* Must start with shebang (#!) follow by the path of the interpreter
  * `#! /bin/bash`
  * `#! /bin/zsh`

--

* Can contain slurm options just **after the shebang** but **before the script commands** → `#SBATCH`
```
#SBATCH -o <out_filename> -e <err_filename>
#SBATCH -p <partition>
#SBATCH -mem-per-cpu=<MB value>
```

---

template: content

# Example

```bash
#!/bin/bash

#SBATCH --cpus-per-task=8
#SBATCH --mem=40GB
#SBATCH -p fast
#SBATCH -o bowtie2.out -e bowtie2.err

module load bowtie2/2.3.4.3
srun bowtie2 --threads $SLURM_CPUS_PER_TASK -x hg19 -1 sample_R1.fq.gz -2 sample_R2.fq.gz -S sample_hg19.sam
```

.center[![A job](images/bowtie2.drawio.png)]

---

template: content

# In brief

Submit (not start) the job:
```
$ sbatch myscript.sh
```

View jobs in the cluster queue:
```
$ squeue
```

View only my jobs in the cluster queue:
```
$ squeue -u <username>
```

View only my running jobs steps:
```
$ squeue -u <username> -s
```

---

template: content

# Resource consumption of jobs

`sacct`let you query the SLURM database to get detailed informations about your jobs

View base informations about your jobs for the current day:

```
$ sacct
```

View base information about your jobs within a time period:

```
$ sacct -S 2019-01-01 -E 2019-06-30
```

View CPU time and memory consumption the job with id `<jobid>`:

```
$ sacct --format=JobID,JobName,CPUTime,MaxVMSize -j <jobid>
```

Learn more about format options :

```
$ sacct --helpformat
```

---

template: content

# Job control

## Cancel a job

`scancel <jobid>`: Cancel job with id `<jobid>`

`scancel -u <user>`: Cancel all jobs from user `<user>`

`scancel -n <jobname>`: Cancel all jobs with name `<jobname>`

`scancel -u <user> -p <partition> -t <state>`: Cancel all jobs from user `<user>`, in partition `<partition>`, in state `<state>`
