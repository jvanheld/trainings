class: center, middle

# IFB High Performance computing usage

## DU-Bii 2019

Julien Seiler (julien.seiler@igbmc.fr)<br/>
Jacques van Helden (jacques.van-helden@univ-amu.fr)

.footer[
https://du-bii.github.io/module-1-Environnement-Unix/
]

---

template: content

# What are we going to talk about today?

* What is an ___HPC cluster___ and what is it used for ?
* How to connect to the [___IFB___](http://www.france-bioinformatique.fr)
* The ___SLURM___ Batch system
* Use "module" to load tools

---
template: content

# What are we going to talk about today?

* What is an ___HPC cluster___ (1) and what is it used for ?
* How to connect to the ___IFB___ (2) core cluster
* The ___SLURM___ (3) Batch system
* Use "module" to load tools

.footnote[
1. High-Performance Computing
2. Institut Français de Bioinformatique (<http://www.france-bioinformatique.fr>)
3. Simple Linux Utility for Resource Management
]
