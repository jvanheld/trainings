template: content

# Using `--ntasks`

Example:

```bash
#!/bin/bash
#SBATCH --ntasks=3

INPUTS=(../fastqc/*.fq.gz)
for INPUT in "${INPUTS[@]}"; do
     srun --ntasks=1 fastqc $INPUT &
done
wait
srun --ntasks=1 multiqc .
```
