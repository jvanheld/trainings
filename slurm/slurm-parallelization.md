template: title

# Parallelization

---

template: content

# How to parallelize ?

SLURM offers several way to parallelize your processing.


**Multithreading**<br/>
*One process can run on several CPU using threads*

- TopHat: `-p` / `--num-threads`
- Bowtie2: `-p` / `--threads`
- Trinity: `--CPU`
- CLC Assembly Cell: `--cpus`

<br/>
.callout.callout-warning[Multithreading is not possible with all software]

<br/><br/>

**Multiprocessing (or multitasking)**<br/>
*Several process of the same software can run in parallel*

---

template: content

# Common parallelization patterns

## Input data splitting

.center[![Input data splitting pattern](images/input_splitting_pattern.drawio.png)]

---

template: content

# Common parallelization patterns

## Variables exploration

.center[![Variable exploration pattern](images/multiparams_pattern.drawio.png)]

---

template: content

# Using `--array`

Example:

.left-column[
`fastqc.sh`
```bash
#!/bin/bash
#SBATCH --array=0-29  # 30 jobs

INPUTS=(../fastqc/*.fq.gz)

srun fastqc ${INPUTS[$SLURM_ARRAY_TASK_ID]}
```

```
$ sbatch fastqc.sh
Submitted batch job 3161045
```

`--array=1,2,4,8`<br/>
`--array=0,100:5 # equivalent to 5,10,15,20...`<br/>
`--array=1-50000%200 # 200 jobs max at the time`

]

--

.right-column[
`multiqc.sh`
```bash
#!/bin/bash

srun multiqc .

```

```
$ sbatch --dependency=afterok:3161045 multiqc.sh
```
]
